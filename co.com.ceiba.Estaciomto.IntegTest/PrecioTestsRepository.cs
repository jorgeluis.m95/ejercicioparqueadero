﻿using System;
using System.Collections.Generic;
using System.Text;
using co.com.ceiba.Estaciomto.Infra;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using co.com.ceiba.Estaciomto.IntegTest;
using co.com.ceiba.Estaciomto.Builders;

namespace co.com.ceiba.Estaciomto.AppTest.AppIntegrationTest
{
    [TestClass]
    public class PrecioTestsRepository
    {
        private EstacionamientoDBContext _contexto;
        public PrecioTestsRepository()
        {
            
        }


        [TestInitialize]
        public void inicializate()
        {
            var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkSqlServer().BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
            builder.UseInMemoryDatabase(databaseName: "ParqueaderoTest");// UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Parqueadero3;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;");

            _contexto = new EstacionamientoDBContext(builder.Options);

            var preciosCarro = new PrecioDataBuilder().buildPreciosCarros();
            var preciosMoto = new PrecioDataBuilder().buildPreciosMoto();

            _contexto.Precios.Add(preciosCarro);
            _contexto.Precios.Add(preciosMoto);
            _contexto.SaveChanges();
        }

        /* Test para validar que el tipo de resultado de la operación devuelva un valor de tipo double, para el valor 
         día carro*/
        [TestMethod]
        public void ObtenerPrecioDiaCarroIntegrationTest()
        {
            // Average
            var tipoCarroTest = "Carro";

            // Act
            double resultTest = _contexto.Precios.Where(x=> x.TipoVehiculo == tipoCarroTest)
                .Select(t=> t.PrecioDia).FirstOrDefault();

            // Assert

            Assert.AreEqual(resultTest.GetType(), typeof(double));
        }

        /* Test para validar que el tipo de resultado de la operación devuelva un valor de tipo double, para el valor 
        Hora carro*/
        [TestMethod]
        public void ObtenerPrecioHoraCarroIntegrationTest()
        {
            // Average
            var tipoCarroTest = "Carro";

            // Act
            double resultTest = _contexto.Precios.Where(x => x.TipoVehiculo == tipoCarroTest)
                .Select(t => t.PrecioHora).FirstOrDefault();

            // Assert

            Assert.AreEqual(resultTest.GetType(), typeof(double));
        }

        /* Test para validar que el tipo de resultado de la operación devuelva 500, que es el valor hora 
         Moto en la BD*/
        [TestMethod]
        public void ObtenerPrecioHoraMotoIntegrationTest()
        {
            // Average
            var tipoCarroTest = "Moto";

            // Act
            double resultTest = _contexto.Precios.Where(x => x.TipoVehiculo == tipoCarroTest)
                .Select(t => t.PrecioHora).FirstOrDefault();

            // Assert
            Assert.AreEqual(500, resultTest);
        }

    }
}
