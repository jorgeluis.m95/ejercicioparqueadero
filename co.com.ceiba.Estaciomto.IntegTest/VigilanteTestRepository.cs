﻿using System;
using System.Collections.Generic;
using System.Text;
using co.com.ceiba.Estaciomto.Infra;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using co.com.ceiba.Estaciomto.AppTest;
using co.com.ceiba.Estaciomto.Domain;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using co.com.ceiba.Estaciomto.Builders;
using co.com.ceiba.Estaciomto.App;

namespace co.com.ceiba.Estaciomto.IntegTest
{
    [TestClass]
    public class VigilanteTestRepository
    {

        private EstacionamientoDBContext _contexto;
        private ViglanteRepository _vigilanteRepository;
        private AppVigilante _AppVigilante;
        private ParqueaderoRepository _ParqueaderoRepository;
        public VigilanteTestRepository()
        {

        }

        [TestInitialize]
        public void InicialiceTests()
        {
            var serviceProvider = new ServiceCollection()
               .AddEntityFrameworkSqlServer().BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
            builder.UseInMemoryDatabase(databaseName: "ParqueaderoTest");
            _contexto = new EstacionamientoDBContext(builder.Options);

            _vigilanteRepository = new ViglanteRepository(builder);
            _ParqueaderoRepository = new ParqueaderoRepository(builder);
            _AppVigilante = new AppVigilante(_vigilanteRepository, _ParqueaderoRepository);

            var vehiuculoTest1 = new VehiculoDataBuilder().BuildCarro();
            vehiuculoTest1.Placa = "DTQ786";
            vehiuculoTest1.Marca = "BMW";
            _contexto.Vehiculos.Add(vehiuculoTest1);

            var vehiculoTest2 = new VehiculoDataBuilder().BuildMoto();
            _contexto.Vehiculos.Add(vehiculoTest2);

            _contexto.SaveChanges();

            for (int i = 0; i < 5; i++)
            {
                var ReservaTest = new ReservaEstacionamientoDataBuilder().build();
                ReservaTest._vehiculo.Placa += i.ToString();
                _contexto.Reservasestacionamiento.Add(ReservaTest);
                _contexto.SaveChanges();

            }
        }


        [TestMethod]
        public void ObtenerVehiculoXPlacaTestReturnNull()
        {
            // Arrange
            var placaTest = "DTQ785";
            // Act
            var vehiucloResultTest = _AppVigilante.ObtenerVehiculoXPlaca(placaTest);

            // Assert
            Assert.AreEqual(null, vehiucloResultTest);
        }

        [TestMethod]
        public void ObtenerVehiculoXPlacaTestReturVehiculo()
        {
            // Arrange
            var placaTest = "DTQ786";
            // Act
            var vehiucloResultTest = _AppVigilante.ObtenerVehiculoXPlaca(placaTest);

            // Assert
            Assert.AreEqual("BMW", vehiucloResultTest.Marca);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GuardarVehiculoReturnExceptionDiaNoPermitido()
        {
            // Arrange
            var vehiculoTest = new VehiculoDataBuilder().BuildCarro();
            vehiculoTest.Placa = "ATQ234";
            var fechaIngresoTest = DateTime.Now;
            // Act
            _AppVigilante.GuardarVehiculo(vehiculoTest, fechaIngresoTest);

            // Assert

        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GuardarVehiculoReturnExceptionVehiculoYaIngresado()
        {
            // Arrange
            var vehiculoTest = new VehiculoDataBuilder().BuildCarro();
            vehiculoTest.Placa = "FTQ2341";
            var fechaIngresoTest = DateTime.Now;
            // Act
            _AppVigilante.GuardarVehiculo(vehiculoTest, fechaIngresoTest);

            

           // Assert


        }


        [TestMethod]
        public void ListarTodasReservasTest()
        {
            // Arrange
            var listaReservas = new List<ReservaEstacionamiento>();

            // Act
            listaReservas = _AppVigilante.ObtenerReservas();

            // Assert
            Assert.AreEqual(5, listaReservas.Count);

        }

        [TestMethod]
        public void AplicarRecargoMotoSiCilindrajeMayor500Return0()
        {
            // Arrange
            var tipoVehiucloTest = "Moto";
            var cilindrajeTest = 125;
            // Act
            var recargoTest = _AppVigilante.AplicarRecargoMotoSiCilindrajeMayor500(tipoVehiucloTest, cilindrajeTest);

            // Assert
            Assert.AreEqual(0, recargoTest);
        }
        [TestMethod]
        public void AplicarRecargoMotoSiCilindrajeMayor500Return200()
        {
            // Arrange
            var tipoVehiucloTest = "Moto";
            var cilindrajeTest = 650;
            // Act
            var recargoTest = _AppVigilante.AplicarRecargoMotoSiCilindrajeMayor500(tipoVehiucloTest, cilindrajeTest);

            // Assert
            Assert.AreEqual(2000, recargoTest);
        }

        [TestMethod]
        public void GenerarCobroMotoTest()
        {
            // Arrange
            var tipoVehiuculo = "Moto";
            var tiempoParqueo = 27;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(5500, cobroTest);
        }
        [TestMethod]
        public void GenerarCobroXDiaMotoTest()
        {
            // Arrange
            var tipoVehiuculo = "Moto";
            var tiempoParqueo = 15;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(4000, cobroTest);
        }

        [TestMethod]
        public void GenerarCobroXHorasMotoTest()
        {
            // Arrange
            var tipoVehiuculo = "Moto";
            var tiempoParqueo = 7;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(3500, cobroTest);
        }

        [TestMethod]
        public void GenerarCobroCarroTest()
        {
            // Arrange
            var tipoVehiuculo = "Carro";
            var tiempoParqueo = 27;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(11000, cobroTest);
        }
        [TestMethod]
        public void GenerarCobroXDiaCarroTest()
        {
            // Arrange
            var tipoVehiuculo = "Carro";
            var tiempoParqueo = 19;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(8000, cobroTest);
        }
        [TestMethod]
        public void GenerarCobroXHorasCarroTest()
        {
            // Arrange
            var tipoVehiuculo = "Carro";
            var tiempoParqueo = 6;

            //act
            var cobroTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiuculo);

            //Assert
            Assert.AreEqual(6000, cobroTest);
        }

        [TestMethod]
        public void LoginVigilanteReturnNull()
        {
            // arrange
            var usuarioTest = "Admon";
            var contraseñaTest = "ad123";
            // Act
            var VigilanteLoginTest = _AppVigilante.LoginVigilante(usuarioTest, contraseñaTest);

            // Assert
            Assert.AreEqual(null, VigilanteLoginTest);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void RegistrarSalidaTestReturnException()
        {
            // Arrange
            var IdParqueVehiuclo = Guid.NewGuid();
            var fechaHoraSalida = DateTime.Now;

            // Act
            var registroSalidaTest = _AppVigilante.registrarSalida(IdParqueVehiuclo.ToString(), fechaHoraSalida);

            // Assert
            Assert.AreEqual(null, registroSalidaTest);
        }

        [TestMethod]
        public void RegistrarSalidaTestReturnReservaParuqeoActualizada()
        {
            // Arrange
            var IdParqueVehiuclo = "";
            var fechaHoraSalida = DateTime.Now;

            // Act
            var reservaParqueoEncontrada = _contexto.Reservasestacionamiento.First();
            IdParqueVehiuclo = reservaParqueoEncontrada.Id.ToString();
            var registroSalidaTest = _AppVigilante.registrarSalida(IdParqueVehiuclo, fechaHoraSalida);


            // Assert
            Assert.AreEqual(typeof(ReservaEstacionamiento), registroSalidaTest.GetType());
        }

        [TestCleanup]
        public void Cleanup()
        {
            _contexto.Database.EnsureDeleted();
            _contexto.SaveChanges();
        }

    }
}
