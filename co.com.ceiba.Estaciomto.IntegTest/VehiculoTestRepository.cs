﻿using co.com.ceiba.Estaciomto.Builders;
using co.com.ceiba.Estaciomto.Domain;
using co.com.ceiba.Estaciomto.Infra;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace co.com.ceiba.Estaciomto.IntegTest
{
    [TestClass]
    public class VehiculoTestRepository
    {
        private EstacionamientoDBContext _contexto;
        public VehiculoTestRepository()
        {
            
        }

        [TestInitialize]
        public void Inicialize()
        {
            // se crea el contexto de la BD en memoria con EF, para relaizar las prueas de integración
            
            var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkSqlServer().BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
            builder.UseInMemoryDatabase(databaseName: "ParqueaderoTest");

            _contexto = new EstacionamientoDBContext(builder.Options);

            // Se agregan valores a la tabla de vehiculos en la base de datos en memoria
            for (int i = 0; i < 2; i++)
            {
                var VehiucloCarroTest = new VehiculoDataBuilder().BuildCarro();
                _contexto.Vehiculos.Add(VehiucloCarroTest);
                _contexto.SaveChanges();
            }

            for (int i = 0; i < 2; i++)
            {
                var vehiculoMotoTest = new VehiculoDataBuilder().BuildMoto();
                _contexto.Vehiculos.Add(vehiculoMotoTest);
                _contexto.SaveChanges();
            }

        }

        [TestMethod]
        public void ObtenerVehiculoXPlacaReturnNull()
        {
            // Arrange
            var PlacaTest = "ZZZ222";

            // Act
            var resultTes = _contexto.Vehiculos.FirstOrDefault(v => v.Placa == PlacaTest);

            // Assert
            Assert.AreEqual(null, resultTes);

        }

        [TestMethod]
        public void ObtenerVehiculoXPlacaReturnVehiculo()
        {
            // Arrange
            var PlacaTest = "FQC578";
            var vehiculoTest = new VehiculoDataBuilder().BuildCarro();
            vehiculoTest.Placa = PlacaTest;
            _contexto.Vehiculos.Add(vehiculoTest);
            _contexto.SaveChanges();

            // Act
            var vehiculoResult = _contexto.Vehiculos.FirstOrDefault(v => v.Placa == PlacaTest);

            // Assert
            Assert.AreEqual(vehiculoResult.GetType(), typeof(Vehiculo));
        }
    }
}
