using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;

namespace Co.com.ceiba.Estaciomto.FuncTest
{
    [TestClass]
    public class SeleniumTests
    {
        [TestMethod]
        public void TestCreateReserva()
        {
            using (var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)))
            {
               
                driver.Navigate().GoToUrl
                 (@"http://localhost:4200/");

                
                var placaCajaTexto = driver.FindElementById("PlacaVehiuclo");
                placaCajaTexto.SendKeys("Esto es una placa: DER6128");
                WebDriverWait wait = new WebDriverWait(driver,TimeSpan.FromSeconds(1));

                var responsable = driver.FindElementByName("ResponsableVehiculo");
                responsable.SendKeys("Santiago");
                var marca = driver.FindElementByName("Marca");
                marca.SendKeys("Santiago");
                var modelo = driver.FindElementByName("Modelo");
                modelo.SendKeys("2011");
                var color = driver.FindElementByName("Color");
                color.SendKeys("Azul");
                var cilindraje = driver.FindElementByName("Cilindraje");
                cilindraje.SendKeys("0");
                var tipoVehiuclo = driver.FindElementByName("RadioTipo");
                tipoVehiuclo.SendKeys("Carro");
               
                var btnReservar = driver.FindElementById("btnReservar");
                btnReservar.Click();

            }
        }
    }
}
