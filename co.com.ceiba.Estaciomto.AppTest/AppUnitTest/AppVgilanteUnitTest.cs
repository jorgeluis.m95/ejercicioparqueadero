using Microsoft.VisualStudio.TestTools.UnitTesting;
using co.com.ceiba.Estaciomto.App;
using co.com.ceiba.Estaciomto.Domain;
using co.com.ceiba.Estaciomto.Builders;
using System;
using System.Threading.Tasks;
using Moq;
using System.Collections.Generic;

namespace co.com.ceiba.Estaciomto.AppTest
{
    [TestClass]
    public class AppVgilanteUnitTest
    {
        private AppVigilante _AppVigilante;
        private Mock<IAppVigilante> _IappVigilanteMock;
        private Mock<IVigilante> _Ivigilante;
        private Mock<IVehiculo> _IVehiculo;
        private Mock<IParqueadero> _IParqueadero;
        public AppVgilanteUnitTest()
        {

        }

        [TestInitialize]
        public void setup()
        {
            _IappVigilanteMock = new Mock<IAppVigilante>();
            _IVehiculo = new Mock<IVehiculo>();
            _Ivigilante = new Mock<IVigilante>();
            _IParqueadero = new Mock<IParqueadero>();
            _AppVigilante = new AppVigilante(_Ivigilante.Object, _IParqueadero.Object);
        }


        /* validar dia de la semana cuando se ingresa la fecha 2/07/2018 da resultad  Lunes 2 de julio  */
        [TestMethod]
        public void ValidarDiaSemanaLunesUnitTest()
        {
            // arrange

            var fechaProbar = "7/02/2018 11:59:59 PM";
            var dateTest = Convert.ToDateTime(fechaProbar);
            _IappVigilanteMock.Setup(r => r.ValidarDiaSemana(dateTest)).Returns("Monday");

            // Act
            var diasemanaDevuelto = _AppVigilante.ValidarDiaSemana(dateTest);

            // Assert
            Assert.AreEqual("Monday", diasemanaDevuelto);
        }

        [TestMethod]
        public void validarDiasemanaDiferenteLunes()
        {
            // arrange
            var fechaProbar = "7/04/2018 11:59:59 AM";
            var dateTest = Convert.ToDateTime(fechaProbar);


            // Act
            var diasemanaDevuelto = _AppVigilante.ValidarDiaSemana(dateTest);

            // Assert
            Assert.AreEqual("Wednesday", diasemanaDevuelto);
        }

        /* test para validar que se devuelve el primer caracter de una placa para un vehiculo */
        [TestMethod]
        public void ObtenerPrimerCaracterPlacasUnitTest()
        {
            // arrange
            var placaTest = "AQG478";

            //Act
            var caracterDevuelto = _AppVigilante.ObtenerPrimerLetraDePlacaIngresada(placaTest);

            // assert
            Assert.AreEqual(1, caracterDevuelto.Length);
        }

        /* Test para calcular el tiempo de parqueo de un vehiculo */
        [TestMethod]
        public void caluclarTiempoParqueoUnitTest()
        {
            // Arrange
            var fechaHoraIngresoTest = DateTime.Parse("6/21/2018 9:12:11 AM");
            var fechaHoraSalidaTest = DateTime.Parse("6/22/2018 11:12:11 PM");

            // Act
            var tiempoParqueoTest = _AppVigilante.caluclarTiempoParqueo(fechaHoraSalidaTest, fechaHoraIngresoTest);


            // Assert
            Assert.AreEqual(38, tiempoParqueoTest);

        }

        [TestMethod]
        public void GurardarVehiculoUnitTest()
        {
            // Arrange
            var vehiuculoTest = new VehiculoDataBuilder().BuildMoto();
            var fechaIngresoTest = DateTime.Now;

            // Act
            try
            {
                _AppVigilante.GuardarVehiculo(vehiuculoTest, fechaIngresoTest);
            }
            catch (Exception err)
            {
                // Assert
                Assert.Fail("error", err.Message);
            }
                      
        }

        [TestMethod]
        public void GuardarVehiculoUnitTestWithExceptionDiaNoPermitido()
        {
            // Arrange
            var vehiuculoTest = new VehiculoDataBuilder().BuildMoto();
            var fechaIngresoTest = DateTime.Now;
            vehiuculoTest.Placa = "ADQ479";
            // Act
            try
            {
                _AppVigilante.GuardarVehiculo(vehiuculoTest, fechaIngresoTest);
            }
            catch (Exception err)
            {
                Assert.AreEqual("D�a no permitido",err.Message);
            }

        }

        [TestMethod]
        public void ValidarSiVehiculoYaEstaIngresado()
        {
            // Arrange
            var placaVehiculoTest = "KER234";

            // Act
            try
            {
                _AppVigilante.ValidarSiVehiculoYaEstaIngresado(placaVehiculoTest);
            }
            catch (Exception err)
            {
                // Assert

                Assert.AreEqual("El vehiculo ya esta ingresado en el parqueadero", err.Message);
            }
        }

        [TestMethod]
        public void GenerarCobroXtipoVehiculoUnitTest()
        {
            // Arrange
            var tipoVehiculoTest = "Moto";
            var tiempoParqueo = 27;
            

            // Act
            var retulTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiculoTest);

            // Assert
            Assert.AreEqual(5500, retulTest);
        }


        [TestMethod]
        public void GenerarCobroXHorasXtipoVehiculoUnitTest()
        {
            // Arrange
            var tipoVehiculoTest = "Moto";
            var tiempoParqueo = 7;


            // Act
            var retulTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiculoTest);

            // Assert
            Assert.AreEqual(3500, retulTest);
        }
        [TestMethod]
        public void GenerarCobroXHorasYDiaXtipoVehiculoUnitTest()
        {
            // Arrange
            var tipoVehiculoTest = "Moto";
            var tiempoParqueo = 37;

            // Act
            var retulTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiculoTest);

            // Assert
            Assert.AreEqual(8000, retulTest);
        }

        [TestMethod]
        public void AplicarRecargoMotoSiCilindrajeMayor500UnitTest()
        {
            // arrange
            var tipoVehiucloTest = "Moto";
            var cilindraje = 510;

            // act
            var recargoTest = _AppVigilante.AplicarRecargoMotoSiCilindrajeMayor500(tipoVehiucloTest, cilindraje);

            // Assert
            Assert.AreEqual(2000, recargoTest);
        }

        [TestMethod]
        public void GenerarCobroXHorasXtipoVehiculoCarroUnitTest()
        {
            // Arrange
            var tipoVehiculoTest = "Carro";
            var tiempoParqueo = 7;


            // Act
            var retulTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiculoTest);

            // Assert
            Assert.AreEqual(7000, retulTest);
        }
        [TestMethod]
        public void GenerarCobroXHorasYDiaXtipoVehiculoCarroUnitTest()
        {
            // Arrange
            var tipoVehiculoTest = "Carro";
            var tiempoParqueo = 37;

            // Act
            var retulTest = _AppVigilante.generarCobro(tiempoParqueo, tipoVehiculoTest);

            // Assert
            Assert.AreEqual(16000, retulTest);
        }

        [TestMethod]
        public void ObtenerReservasUnitTest()
        {
            // Arrange
            var listaReservas = new List<ReservaEstacionamiento>();
            _IappVigilanteMock.Setup(x => x.ObtenerReservas()).Returns(listaReservas);

            //Act
            var objectResult = _IappVigilanteMock.Object;
            var resultTest = objectResult.ObtenerReservas();

            // Assert

            Assert.AreEqual(0, resultTest.Count);
        }

        [TestMethod]
        public void ObtenerVehiucloXPlaca()
        {
            // Arrange
            var vehiuculoTest = new VehiculoDataBuilder().BuildCarro();
            var placaTest = "QXT346";
            _IappVigilanteMock.Setup(x => x.ObtenerVehiculoXPlaca(placaTest)).Returns(vehiuculoTest);

            // Act
            var objectResult = _IappVigilanteMock.Object;
            var resultTest = objectResult.ObtenerVehiculoXPlaca(placaTest);

            // Assert
            Assert.AreEqual("BMW", resultTest.Marca);

        }

    }
}
