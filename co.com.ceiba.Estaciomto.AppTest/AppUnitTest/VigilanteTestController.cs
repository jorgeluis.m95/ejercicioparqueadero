﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estacionamiento.Api;
using co.com.ceiba.Estaciomto.App;
using co.com.ceiba.Estacionamiento.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using co.com.ceiba.Estaciomto.Builders;
using AutoMapper;
namespace co.com.ceiba.Estaciomto.UnitTest
{
    [TestClass]
    public class VigilanteTestController
    {
        private readonly HelperResponse _helperResponse;

        public VigilanteTestController()
        {
            _helperResponse = new HelperResponse();
        }


        [TestMethod]
        public async Task crearReservaReturnOk()
        {
            // Arrange
            var IaaVigilante = new Mock<IAppVigilante>();
            
            var vigilanteController = new VigilanteController(IaaVigilante.Object,_helperResponse);

            var vehiucloTest = new VehiculoDataBuilder().BuildCarro();

            // Act
            var actionresultTest = await vigilanteController.IngresarVehiuclo(vehiucloTest);
            var resultTest = actionresultTest as OkObjectResult;
            // Assert
            Assert.AreEqual(200,resultTest.StatusCode);
        }


        [TestMethod]
        public async Task GetAllReservasReturnOk()
        {
            // Arrange
            
            var IaaVigilante = new Mock<IAppVigilante>();
            
            var vigilanteController = new VigilanteController(IaaVigilante.Object, _helperResponse);

            // Act
            var actionResultTest = await vigilanteController.GetallReservas();
            var resultTest = actionResultTest as NotFoundObjectResult;

            // Assert
            Assert.IsNotNull(resultTest);
        }

        [TestMethod]
        public async Task GetAllReservasReturnNotFound()
        {
            // Arrange
            var IaaVigilante = new Mock<IAppVigilante>();
            var vigilanteController = new VigilanteController(IaaVigilante.Object,_helperResponse);

            // Act
            var actionResult = await vigilanteController.GetallReservas();
            var testResult = actionResult as NotFoundObjectResult;

            // Assert
            Assert.AreEqual(404,testResult.StatusCode.Value);
            Assert.IsNotNull(testResult);
        }
    }
}
