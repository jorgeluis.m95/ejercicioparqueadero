﻿using co.com.ceiba.Estaciomto.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Builders
{
    public class PrecioDataBuilder
    {
        public Guid Id { get; set; }

        public Double PrecioDia { get; set; }

        public Double PrecioHora { get; set; }

        public String TipoVehiculo { get; set; }

        public PrecioDataBuilder()
        {
            this.Id = Guid.NewGuid();
            this.PrecioDia = 1000;
            this.PrecioHora = 500;
            this.TipoVehiculo = "Moto";

        }

        public Precio buildPreciosCarros()
        {
            return new Precio
            {
                Id = this.Id,
                PrecioDia = 8000,
                PrecioHora = 1000,
                TipoVehiculo = "Carro"
            };
        }
        public Precio buildPreciosMoto()
        {
            return new Precio
            {
                Id = this.Id,
                PrecioDia = this.PrecioDia,
                PrecioHora = this.PrecioHora,
                TipoVehiculo = this.TipoVehiculo
            };
        }
    }
}
