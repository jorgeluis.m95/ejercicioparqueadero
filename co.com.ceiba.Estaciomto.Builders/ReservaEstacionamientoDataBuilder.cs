﻿using co.com.ceiba.Estaciomto.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Builders
{
    public class ReservaEstacionamientoDataBuilder
    {
        public Guid Id { get; set; }
        public Guid ParqueaderoId { get; set; }

        public Parqueadero _Parqueadero { get; set; }
        public Guid VehiculoId { get; set; }

        public Vehiculo _vehiculo { get; set; }

        public DateTime FechaIngreso { get; set; }

        public DateTime FechaSalida { get; set; }

        public float TiempoParqueo { get; set; }

        public Double CostoTotal { get; set; }


        public ReservaEstacionamientoDataBuilder()
        {
            this.Id = Guid.NewGuid();
            this.ParqueaderoId = Guid.NewGuid();
            this.VehiculoId = Guid.NewGuid();
            this._vehiculo = new Vehiculo
            {
                Id = Guid.NewGuid(),
                Placa = "FTQ234",
                Modelo = "2012",
                Color = "Blaco",
                TipoVehiculo = "Carro",
                ListReservaEstacionamientos = null
            };
            this._Parqueadero = new Parqueadero
            {
                Id = Guid.NewGuid(),
                Direccion = "cll15",
                Ciudad = "Medellin",
                ListReservaEstacionamientos = null,
                Vigilantes = null

            };
            FechaIngreso = DateTime.Now;
            TiempoParqueo = 0;
            CostoTotal = 0;
        }

        public ReservaEstacionamiento build()
        {
            return new ReservaEstacionamiento
            {
                Id = this.Id,
                ParqueaderoId = this.ParqueaderoId,
                VehiculoId = this.VehiculoId,
                _vehiculo = this._vehiculo,
                _Parqueadero = this._Parqueadero,
                FechaIngreso = this.FechaIngreso,
                TiempoParqueo = this.TiempoParqueo,
                CostoTotal = this.CostoTotal
            };
        }
    }
}
