﻿using co.com.ceiba.Estaciomto.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Builders
{
    public class VehiculoDataBuilder
    {
        public Guid Id { get; set; }
        public String Color { get; set; }

        public String Placa { get; set; }

        public String Marca { get; set; }

        public String Modelo { get; set; }

        public String TipoVehiculo { get; set; }
        public String NombreDuenoCarro { get; set; }

        public int Cilindraje { get; set; }
        public virtual ICollection<ReservaEstacionamiento> ListReservaEstacionamientos { get; set; }

        public VehiculoDataBuilder()
        {
            this.Id = Guid.NewGuid();
            this.Placa = "GTF345";
            this.Marca = "BMW";
            this.Modelo = "2011";
            this.TipoVehiculo = "Carro";
            this.Cilindraje = 0;
            this.Color = "Azul";
            this.ListReservaEstacionamientos = null;
            ;
        }

        public Vehiculo BuildCarro()
        {
            return new Vehiculo
            {
                Id = this.Id,
                Placa = this.Placa,
                Marca = this.Marca,
                Modelo = this.Modelo,
                Cilindraje = this.Cilindraje,
                Color = this.Color,
                ListReservaEstacionamientos = this.ListReservaEstacionamientos,
                NombreDuenoCarro = "Ivan",
                TipoVehiculo = "Carro"

            };
        }

        public Vehiculo BuildMoto()
        {
            return new Vehiculo
            {
                Id = this.Id,
                Placa = this.Placa + "M",
                Marca = this.Marca,
                Modelo = this.Modelo,
                Cilindraje = 180,
                Color = this.Color,
                ListReservaEstacionamientos = this.ListReservaEstacionamientos,
                NombreDuenoCarro = "Ivan",
                TipoVehiculo = "Moto"
            };
        }
    }
}
