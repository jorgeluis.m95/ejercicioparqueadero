﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estaciomto.Domain;

namespace co.com.ceiba.Estaciomto.App
{
    public interface IAppVigilante 
    {

        String ObtenerPrimerLetraDePlacaIngresada(String placa);
        String ValidarDiaSemana(DateTime fechaingreso);
        ReservaEstacionamiento registrarSalida(string Id, DateTime fechaHoraSalida);
        List<ReservaEstacionamiento> ObtenerReservas();
        Vehiculo ObtenerVehiculoXPlaca(String placa);
        double generarCobro(double tiempoParqueo, string tipoVehiculo);
        double caluclarTiempoParqueo(DateTime fechaHoraSalida, DateTime fechaHoraIngreso);
        void GuardarVehiculo(Vehiculo vehiculo, DateTime fechaIngreso);
        void RegistrarVigilante(Vigilante vigilante);

        Vigilante LoginVigilante(string usuario, string contraseña);
    }
}
