﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estaciomto.Domain;
using co.com.ceiba.Estaciomto.Domain.Entities;

namespace co.com.ceiba.Estaciomto.App
{
    public class AppVigilante : IAppVigilante

    {

        private readonly IVigilante _Ivigilante;
        private readonly IParqueadero _IParqueadero;

        public AppVigilante(IVigilante IVigilante,IParqueadero Iparqueadero)
        {
         
            _Ivigilante = IVigilante;
            _IParqueadero = Iparqueadero;
        }

        public void GuardarVehiculo(Vehiculo vehiculo, DateTime fechaIngreso)
        {
            try
            {
                ValidarSiVehiculoYaEstaIngresado(vehiculo.Placa);
                validarCupoParqueadero(vehiculo.TipoVehiculo);
                validarPlacaYDiaSemanaVehiuclo(vehiculo.Placa, fechaIngreso);   
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            GenerarObjetoReservaParaPasarInsertarEnBd(vehiculo,fechaIngreso);   
        }

        public void GenerarObjetoReservaParaPasarInsertarEnBd(Vehiculo vehiculo, DateTime fehcaIngreso)
        {
            var generarNuevaReservaEstacionnamiento = new ReservaEstacionamiento();
            
            var vehiculoresult = ObtenerVehiculoXPlaca(vehiculo.Placa);
            if (vehiculoresult != null)
            {
                generarNuevaReservaEstacionnamiento.VehiculoId = vehiculoresult.Id;
                generarNuevaReservaEstacionnamiento._vehiculo = null;
            }
            else
            {
                vehiculo.Id = Guid.NewGuid();
                _Ivigilante.RegistrarVehiculo(vehiculo);
                generarNuevaReservaEstacionnamiento.VehiculoId = vehiculo.Id;
                generarNuevaReservaEstacionnamiento._vehiculo = null;

            }
            generarNuevaReservaEstacionnamiento.Id = Guid.NewGuid();
            generarNuevaReservaEstacionnamiento.FechaIngreso = fehcaIngreso;
            generarNuevaReservaEstacionnamiento.FechaSalida = DateTime.Parse("0001-01-01T00:00:00");
            generarNuevaReservaEstacionnamiento.ParqueaderoId = Guid.Parse("48c210c0-5f60-46ec-ae10-47dc004a6376");

            _IParqueadero.IsertarReservaEstacionamientoEnBD(generarNuevaReservaEstacionnamiento);

        }
        public void ValidarSiVehiculoYaEstaIngresado(string placavehiculo)
        {
            try
            {
                var parqueoVehiculoEncontrado = _IParqueadero.ObtenerParqueoDeVehiculoXplaca(placavehiculo);
                if(parqueoVehiculoEncontrado != null)
                {
                    throw new Exception("El vehiculo ya esta ingresado en el parqueadero");
                }              
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }

        }

  
        public string ValidarDiaSemana(DateTime fechaingreso)
        {
            try
            {
                var diaSemana = fechaingreso.DayOfWeek.ToString();
                return diaSemana;
            }
            catch (InvalidOperationException e)
            {
                return e.Message;
            }
        }

        public string ObtenerPrimerLetraDePlacaIngresada(string placa)
        {
            var letraInicialPlaca = placa.ToCharArray();
            return Convert.ToString(letraInicialPlaca[0]);
        }
        
        public void validarPlacaYDiaSemanaVehiuclo(string Placa, DateTime fechadeIngreso)
        {
            var LetraDeRestriccionPlaca = "A";
            try
            {
                var inicialPlaca = ObtenerPrimerLetraDePlacaIngresada(Placa);
                var diaSemana = ValidarDiaSemana(fechadeIngreso);
                if (inicialPlaca == LetraDeRestriccionPlaca && (diaSemana != DayOfWeek.Monday.ToString() ||
                diaSemana != DayOfWeek.Sunday.ToString()))
                {
                    throw new Exception("Día no permitido");
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }

        }


        public  void validarCupoParqueadero(string tipoVehiuclo)
        {
            CeldaFact celda = FactoryCeldas.obtenerCeldas(tipoVehiuclo);
            try
            {
                var numeroVehiuclosActualesXtipo = _IParqueadero.ObtenerNumeroVehiculosActualesEnParqueaderoXTipoVehiculo(tipoVehiuclo);
                if (numeroVehiuclosActualesXtipo == celda.obtenerNumeroCeldas())
                {
                    throw new Exception("No hay cupo");
                }
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }

        public ReservaEstacionamiento registrarSalida(string Id, DateTime fechaHoraSalida)
        {
            try
            {
                var reservaASalir = _IParqueadero.ObtenerReservaById(Guid.Parse(Id));
                var tiempoParqueo = caluclarTiempoParqueo(fechaHoraSalida, reservaASalir.FechaIngreso);
                var cobroTotal = generarCobro(tiempoParqueo, reservaASalir._vehiculo.TipoVehiculo);
                var recargocilindrajeMoto = AplicarRecargoMotoSiCilindrajeMayor500(reservaASalir._vehiculo.TipoVehiculo, reservaASalir._vehiculo.Cilindraje);
                cobroTotal = (cobroTotal + recargocilindrajeMoto);

                // Actualizo la fecha en BD
                reservaASalir.FechaSalida = fechaHoraSalida;
                reservaASalir.CostoTotal = cobroTotal;
                var tiempoGuardar = Math.Ceiling(tiempoParqueo);
                reservaASalir.TiempoParqueo = float.Parse(tiempoGuardar.ToString());
                _IParqueadero.actualizarParqueVehiuclo(reservaASalir);
                return reservaASalir;
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }
   
        }

        public double AplicarRecargoMotoSiCilindrajeMayor500(string tipoVehiuclo, int cilindraje)
        {
            const double recargoAPagarCilindrajeMoto = 2000;
            const int numeroMaximoCilindrajeMotoAntesDeRecargo = 500;
            var _tipoVehiculo = "Moto";
            double recargoAplicado = 0;
            if (tipoVehiuclo == _tipoVehiculo && cilindraje > numeroMaximoCilindrajeMotoAntesDeRecargo)
            {
                recargoAplicado = recargoAPagarCilindrajeMoto;
            }
         
            return recargoAplicado;
        }

        public double caluclarTiempoParqueo(DateTime fechaHoraSalida, DateTime fechaHoraIngreso)
        {
            var tiempoParqueo = (fechaHoraSalida - fechaHoraIngreso);
            return tiempoParqueo.TotalHours;
        }

        public double generarCobro(double tiempoParqueo, string tipoVehiculo)
        {
            double cobroTotal = 0.0;
            double cobroHorasRestantes = 0.0;
            double cobroDias = 0.0;
            const int numeroHorasEnUnDia = 24;
            const int numeroHorasParaCobroXDia = 9;

            PrecioFact precio = FactoryPrecio.getPrecio(tipoVehiculo);
            double tiempoExactoParqueo = Math.Ceiling(tiempoParqueo);
            if ((tiempoExactoParqueo > numeroHorasParaCobroXDia && tiempoExactoParqueo <= numeroHorasEnUnDia))
            {
                // se realiza el cobro x el valor del día
                cobroDias = precio.PrecioXDia();
                cobroTotal = cobroDias;

            }
            else if (tiempoExactoParqueo < numeroHorasParaCobroXDia)
            {
                // se realiza el cobro x horas
                cobroDias = precio.PrecioXHora() * tiempoExactoParqueo;
                cobroTotal = cobroDias;

            }
            else if (tiempoExactoParqueo > numeroHorasEnUnDia)
            {
                // se realiza el cobro x día y x horas 
                var obtenerDías = tiempoExactoParqueo / numeroHorasEnUnDia;
                cobroDias = Math.Floor(obtenerDías) * precio.PrecioXDia();
                var residuoHoras = (tiempoExactoParqueo % numeroHorasEnUnDia);
                if(residuoHoras > numeroHorasParaCobroXDia && residuoHoras < numeroHorasEnUnDia)
                {
                    cobroHorasRestantes = precio.PrecioXDia();
                    cobroTotal = (cobroDias + cobroHorasRestantes);
                }
                else
                {
                    cobroHorasRestantes = (residuoHoras * precio.PrecioXHora());
                    cobroTotal = (cobroDias + cobroHorasRestantes);
                }
 
            }
            return cobroTotal;

        }

        public void RegistrarVigilante(Vigilante vigilante)
        {
            throw new NotImplementedException();
        }

        public Vigilante LoginVigilante(string usuario, string contraseña)
        {
            var vigilanteLogin = _Ivigilante.LoginVigilante(usuario, contraseña);
            return vigilanteLogin;
        }

        public List<ReservaEstacionamiento> ObtenerReservas()
        {
            var listaReservas = _Ivigilante.ObtenerReservas();
            return listaReservas;
        }

        public Vehiculo ObtenerVehiculoXPlaca(string placa)
        {
            try
            {
                var vehiculoEncontrado = _Ivigilante.ObtenerVehiculoXPlaca(placa);
                return vehiculoEncontrado;
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }
            
        }
    }

}
