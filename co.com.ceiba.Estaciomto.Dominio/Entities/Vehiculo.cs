﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public class Vehiculo
    {
        public Guid Id { get; set; }
        public String Color { get; set; }

        public String Placa { get; set; }

        public String Marca { get; set; }

        public String Modelo { get; set; }

        public String TipoVehiculo { get; set; }
        public String NombreDuenoCarro { get; set; }

        public int Cilindraje { get; set; }
        public virtual ICollection<ReservaEstacionamiento> ListReservaEstacionamientos { get; set; }

    }
}
