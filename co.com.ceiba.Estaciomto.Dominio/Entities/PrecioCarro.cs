﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain.Entities
{
    public class PrecioCarro : PrecioFact
    {
        private double precioXDia = 8000;
        private double precioXhora = 1000;

  

        public override double PrecioXDia()
        {
            return precioXDia;
        }

        public override double PrecioXHora()
        {
            return precioXhora;
        }
    }
}
