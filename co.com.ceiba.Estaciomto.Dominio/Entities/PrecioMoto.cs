﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain.Entities
{
    public class PrecioMoto : PrecioFact
    {

        private double precioXHora = 500;
        private double precioXDia = 4000;

        public override double PrecioXDia()
        {
            return precioXDia;
        }

        public override double PrecioXHora()
        {
            return precioXHora;
        }
    }
}
