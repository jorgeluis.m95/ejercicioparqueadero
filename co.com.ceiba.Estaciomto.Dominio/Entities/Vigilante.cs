﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public class Vigilante
    {
        public Guid Id { get;  set; }

        public String Name { get;  set; }

        public String Age { get;  set; }

        public Parqueadero parqueadero { get; set; }

        public string nombreUsuario { get; set; }

        public string Contraseña { get; set; }

    }
}
