﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
   public class ReservaEstacionamiento
    {
        public Guid Id { get; set; }
        public Guid ParqueaderoId { get; set; }

        public Parqueadero _Parqueadero { get; set; }
        public Guid VehiculoId { get; set; }

        public Vehiculo _vehiculo { get; set; }

        public DateTime FechaIngreso { get; set; }

        public DateTime FechaSalida { get; set; }

        public float TiempoParqueo { get; set; }

        public Double CostoTotal { get; set; }
    }
}
