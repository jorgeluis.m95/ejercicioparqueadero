﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace co.com.ceiba.Estaciomto.Domain
{
    public class Parqueadero
    {
    
        public Guid Id { get; set; }
        
        public String Direccion { get; set; }

        public String Ciudad { get; set; }

        public List<Vigilante> Vigilantes { get; set; }
        
        public virtual ICollection<ReservaEstacionamiento> ListReservaEstacionamientos { get; set; }

    }   
}
