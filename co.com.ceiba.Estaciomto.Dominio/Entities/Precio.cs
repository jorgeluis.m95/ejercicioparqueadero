﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public  class Precio
    {
        public Guid Id { get; set; }

        public Double PrecioDia { get; set; }

        public Double PrecioHora { get; set; }

        public String TipoVehiculo { get; set; }


    }


}
