﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain.Entities
{
    public class Login
    {
        public String usuario { get; set; }
        public String contrasena { get; set; }
    }
}
