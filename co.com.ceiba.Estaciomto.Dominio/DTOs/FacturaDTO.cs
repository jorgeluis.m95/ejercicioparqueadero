﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
   public class FacturaDTO
    {
        public double cobroTotal { get; set; }
        public string placa { get; set; }
        public string nombre { get; set; }
    }
}
