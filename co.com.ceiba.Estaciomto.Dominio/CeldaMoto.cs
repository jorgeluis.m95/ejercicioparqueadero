﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain.Entities
{
    public class CeldaMoto : CeldaFact
    {
        private readonly int numeroMaximoCeldasMoto = 10;

        public override int obtenerNumeroCeldas()
        {
            return numeroMaximoCeldasMoto;
        }
    }
}
