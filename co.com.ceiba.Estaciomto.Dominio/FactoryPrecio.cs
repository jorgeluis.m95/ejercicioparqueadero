﻿using co.com.ceiba.Estaciomto.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public class FactoryPrecio
    {

        public static PrecioFact getPrecio(String tipoVehiculo)
        {
            PrecioFact precio = null;
            switch (tipoVehiculo)
            {
                case "Moto":
                    precio = new PrecioMoto();
                    break;
                case "Carro":
                    precio = new PrecioCarro();
                    break;
            }
            return precio;
        }
    }
}
