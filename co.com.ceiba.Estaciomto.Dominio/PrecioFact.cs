﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public abstract class PrecioFact
    {
        public abstract double PrecioXDia();

        public abstract double PrecioXHora();

    }
}
