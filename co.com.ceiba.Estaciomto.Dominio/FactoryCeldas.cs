﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain.Entities
{
    public class FactoryCeldas
    {
        public static CeldaFact obtenerCeldas(string tipoVehiculo)
        {
            CeldaFact celdas = null;
            switch (tipoVehiculo)
            {
                case "Moto":
                    celdas = new CeldaMoto();
                    break;
                case "Carro":
                    celdas = new CeldaCarro();
                    break;
            }
            return celdas;
        }
    }
}
