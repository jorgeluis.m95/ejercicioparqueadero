﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace co.com.ceiba.Estaciomto.Domain
{
    public interface IRepositoryGeneric<T> where T: class
    {
        void Add(T Entitie);

        void Update(T Entitie);

        Task<List<T>> ListAll();

        T GetById(Guid Id);


    }
}
