﻿using System;
using System.Collections.Generic;
using System.Text;
using co.com.ceiba.Estaciomto.Domain;
namespace co.com.ceiba.Estaciomto.Domain
{
    public interface IParqueadero : IRepositoryGeneric<Parqueadero>
    {

        int ObtenerNumeroVehiculosActualesEnParqueaderoXTipoVehiculo(string tipovehiuclo);

        void IsertarReservaEstacionamientoEnBD(ReservaEstacionamiento reserva);
        void actualizarParqueVehiuclo(ReservaEstacionamiento reserva);
        ReservaEstacionamiento ObtenerReservaById(Guid Id);

        ReservaEstacionamiento ObtenerParqueoDeVehiculoXplaca(string placa);
        
    }
}
