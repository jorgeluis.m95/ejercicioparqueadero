﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace co.com.ceiba.Estaciomto.Domain
{
    public interface IReservaEstacionamiento : IRepositoryGeneric<ReservaEstacionamiento>
    {
        String ValidarPlaca(String placa);
        String ValidarDiaSemana(int numeroDia);

        double GenerarCobro(DateTime fechaIngreso, DateTime fechaSalida);

        Task<List<ReservaEstacionamiento>> ObtenerReservas();

        int ObtenerNumeroReservaActualesCarro();
        int ObtenerNumeroReservaActualesMoto();

        ReservaEstacionamiento ObtenerReservaById(Guid Id);

        ReservaEstacionamiento ObtenerReservaDeVehiculo(Guid IdVehiculo);
    }
}
