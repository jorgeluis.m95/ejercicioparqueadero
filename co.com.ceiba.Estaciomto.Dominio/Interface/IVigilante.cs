﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace co.com.ceiba.Estaciomto.Domain
{
    public interface IVigilante
    {
      
        double GenerarCobro(DateTime fechaIngreso, DateTime fechaSalida);
        void RegistrarVehiculo(Vehiculo vehiculo);
        void RegistrarVigilante(Vigilante vigilante);
        Vehiculo ObtenerVehiculoXPlaca(String placa);
        Vigilante LoginVigilante(string usuario, string contraseña);
        List<ReservaEstacionamiento> ObtenerReservas();
    }
}
