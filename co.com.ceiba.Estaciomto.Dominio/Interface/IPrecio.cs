﻿using System;
using System.Collections.Generic;
using System.Text;

namespace co.com.ceiba.Estaciomto.Domain
{
    public interface IPrecio
    {
        Double obtenerPrecioXhora(string tipoVehiculo);

        Double obtenerPrecioXDia(string tipoVehiculo);



    }
}
