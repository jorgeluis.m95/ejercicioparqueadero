﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using dominio =  co.com.ceiba.Estaciomto.Domain;
namespace co.com.ceiba.Estacionamiento.Api
{
    public class AutoMapperProfile : Profile
    {

        public AutoMapperProfile()
        {
            CreateMap<dominio.ReservaEstacionamiento, dominio.ReservaEstacionamientoDto>()
                .ForMember(des => des.CostoTotal, opt => opt.ResolveUsing(src => src.CostoTotal * 3));
        }

    }
}
