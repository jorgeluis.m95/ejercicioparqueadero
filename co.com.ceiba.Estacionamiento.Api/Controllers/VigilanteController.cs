﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using co.com.ceiba.Estaciomto.App;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace co.com.ceiba.Estacionamiento.Api.Controllers
{


    public class VigilanteController : Controller
    {

        private readonly IAppVigilante _IappVigilante;
        private readonly HelperResponse _response;
        public VigilanteController(IAppVigilante IappVeigilante, HelperResponse hhelperResponse)
        {
            _response = hhelperResponse;
            _IappVigilante = IappVeigilante;
        }


        [HttpPost]
        [Route("api/RegistrarVigilante")]
        public IActionResult RegitrarVigilante([FromBody] Vigilante vigilante)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            else
            {
                try
                {
                    _IappVigilante.RegistrarVigilante(vigilante);
                    return Ok();
                }
                catch (Exception ex)
                {
                    _response.Message = ex.Message;

                    return BadRequest(_response);
                }
            }
        }


        [HttpGet]
        [EnableCors("AllowSpecificOrigin")]
        [Route("api/GetallReservas")]
        [Authorize]
        public async Task<IActionResult> GetallReservas()
        {
            try
            {
                var listaReservas =  _IappVigilante.ObtenerReservas();
                foreach (var item in listaReservas)
                {
                    item._vehiculo.ListReservaEstacionamientos = null;
                    item._Parqueadero.ListReservaEstacionamientos = null;

                }
                _response.StatusCode = 200;
                _response.Message = "listado de reservas actuales en el parqueadero";
                _response.Result = listaReservas;
                return Ok(_response);

            }
            catch (Exception e)
            {
                _response.Message = e.Message;
                _response.Result = null;
                _response.StatusCode = 404;
                return NotFound(_response);

            }
        }

        [HttpPost]
        [EnableCors("AllowSpecificOrigin")]
        [Route("api/vigilante/IngresarVehiuclo")]
        [Authorize]

        public async Task<IActionResult> IngresarVehiuclo([FromBody] Vehiculo vehiculo)
        {
            var mensajeReservaGuardada = "Reserva registrada";
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                _IappVigilante.GuardarVehiculo(vehiculo, DateTime.Now);
                _response.StatusCode = 200;
                _response.Message = mensajeReservaGuardada;
                _response.Result = null;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.StatusCode = 400;
                _response.Message = ex.Message;
                _response.Result = null;
                return Ok(_response);
            }

        }

        [HttpGet("{id}")]
        [Route("api/vigilante/RegistrarSalida")]
        [EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> RegistrarSalida(string id)
        {
            try
            {
                var _factura = new FacturaDTO();
                var fechaHorsSalidaVehiculo = DateTime.Now;
                var reservaSaliente = _IappVigilante.registrarSalida(id, fechaHorsSalidaVehiculo);
                _factura.cobroTotal = reservaSaliente.CostoTotal;
                _factura.nombre = reservaSaliente._vehiculo.NombreDuenoCarro;
                _factura.placa = reservaSaliente._vehiculo.Placa;
                _response.StatusCode = 200;
                _response.Message = "Ok";
                _response.Result = _factura;
                return Ok(_response);
            }
            catch (InvalidOperationException e)
            {
                _response.StatusCode = 404;
                _response.Message = "La reserva no fue encontrada en el sistema";
                _response.Result = null;
                return NotFound(_response);

            }
        }

    }
}