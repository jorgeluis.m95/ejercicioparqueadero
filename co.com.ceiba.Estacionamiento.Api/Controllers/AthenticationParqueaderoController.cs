﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estaciomto.App;
using co.com.ceiba.Estaciomto.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace co.com.ceiba.Estacionamiento.Api.Controllers
{
    [Produces("application/json")]
    // [Route("api/AthenticationParqueadero")]
    public class AthenticationParqueaderoController : Controller
    {

        private readonly IConfiguration _config;
        private readonly IAppVigilante _IaaVigilante;
        public AthenticationParqueaderoController(IConfiguration config, IAppVigilante Ivigilante)
        {
            _config = config;
            _IaaVigilante = Ivigilante;
        }

        [HttpPost]
        [Route("api/LoginParqueadero")]
        public IActionResult LoginParqueadero([FromBody] Login userLogin)
        {
           
            var vigilanteLogueado = _IaaVigilante.LoginVigilante(userLogin.usuario, userLogin.contrasena);
            if(vigilanteLogueado != null)
            {
                var tokenVigilante = construirToken();
                return Ok(new { tokenVigilante=tokenVigilante});
            }

            return Ok();
        }
        public string construirToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "http://localhost:63659/",
                audience: "http://localhost:63659/",
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);
            var resultadoToken = new JwtSecurityTokenHandler().WriteToken(token);
            return resultadoToken;
        }
    }
}