﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace co.com.ceiba.Estacionamiento.Api
{
    public class HelperResponse
    {
        public int StatusCode { get; set; }

        public String Message { get; set; }

        public object Result { get; set; }
    }
}
