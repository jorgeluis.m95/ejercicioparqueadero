﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AutoMapper;

using co.com.ceiba.Estaciomto.Domain;
using co.com.ceiba.Estaciomto.Infra;
using co.com.ceiba.Estaciomto.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;


namespace co.com.ceiba.Estacionamiento.Api
{
    public class Startup
    {

        public IConfiguration Configuration { get; }

        public IHostingEnvironment _CurrentEnvironment { get; }
        public Startup(IConfiguration configuration, IHostingEnvironment CurrentEnvironment)
        {
            Configuration = configuration;
            _CurrentEnvironment = CurrentEnvironment;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            // services de capa de aplicación
            services.AddSingleton(typeof(IRepositoryGeneric<>), typeof(RepositoryGeneric<>));
            services.AddSingleton<IReservaEstacionamiento, ReservaEstacionamientoRepository>();
            services.AddSingleton<IAppVigilante, AppVigilante>();

            // services de capa de dominio
            services.AddSingleton<IVehiculo, VehiculoRepository>();
            services.AddSingleton<IPrecio, PrecioRepository>();
            services.AddSingleton<IVigilante, ViglanteRepository>();
            services.AddSingleton<IParqueadero, ParqueaderoRepository>();

            services.AddSingleton(typeof(HelperResponse), typeof(HelperResponse));

            services.AddCors(options => options.AddPolicy("AllowSpecificOrigin",
            builder => builder.WithOrigins("*")));

            services.AddAutoMapper(x => x.AddProfile(new AutoMapperProfile()));
            
            

            services.AddAuthentication(cfg =>
            {
                cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
           
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,

                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "http://localhost:63659/",
                    ValidAudience = "http://localhost:63659/",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                    
                };
                options.SaveToken = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMvc();
           
            
            app.UseCors("AllowSpecificOrigin");
        }
    }
}
