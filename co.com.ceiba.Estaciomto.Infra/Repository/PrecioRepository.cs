﻿using co.com.ceiba.Estaciomto.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace co.com.ceiba.Estaciomto.Infra
{
    public class PrecioRepository : IPrecio
    {
        private readonly DbContextOptionsBuilder<EstacionamientoDBContext> optionsBuilder;

        public PrecioRepository()
        {
            optionsBuilder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
        }

        public double obtenerPrecioXDia(string tipoVehiculo)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(optionsBuilder.Options))
            {
                try
                {
                    var precioDia = contexto.Precios.Where(p => p.TipoVehiculo == tipoVehiculo).Select(p => p.PrecioDia).FirstOrDefault();
                    return precioDia;
                }
                catch (InvalidOperationException e)
                {

                    return 0.0;
                }
            }
               
        }

        public double obtenerPrecioXhora(string tipoVehiculo)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(optionsBuilder.Options))
            {
                try
                {
                    var precioHora = contexto.Precios.Where(p => p.TipoVehiculo == tipoVehiculo).Select(p => p.PrecioHora).FirstOrDefault();
                    return precioHora;
                }
                catch (InvalidOperationException e)
                {

                    return 0.0;
                }
            }
        }
    }
}
