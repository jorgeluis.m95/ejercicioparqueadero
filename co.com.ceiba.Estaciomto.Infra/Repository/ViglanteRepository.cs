﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.EntityFrameworkCore;

namespace co.com.ceiba.Estaciomto.Infra
{
    public class ViglanteRepository : RepositoryGeneric<Vigilante>, IVigilante
    {
        private readonly DbContextOptionsBuilder<EstacionamientoDBContext> _optionBuilder;

        public ViglanteRepository()
        {
            _optionBuilder = new DbContextOptionsBuilder<EstacionamientoDBContext>();

        }

        // constructor para pruebas de integracion
        public ViglanteRepository(DbContextOptionsBuilder<EstacionamientoDBContext> _builder)
        {
            _optionBuilder = _builder;
        }
        public double GenerarCobro(DateTime fechaIngreso, DateTime fechaSalida)
        {
            throw new NotImplementedException();
        }

        public List<ReservaEstacionamiento> ObtenerReservas()
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return contexto.Reservasestacionamiento.Where(x => x.FechaSalida == DateTime.Parse("0001-01-01T00:00:00"))
                    .Include(v => v._vehiculo).Include(v => v._Parqueadero).ToList();

                }
                catch (InvalidOperationException e)
                {

                    return null;
                }

            }
        }
        public void  RegistrarVigilante(Vigilante vigilante)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                contexto.Vigilantes.Add(vigilante);
                contexto.SaveChanges();
            }
        }

        public Vigilante LoginVigilante(string usuario, string contraseña)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                var vigilanteEncontrado = contexto.Vigilantes.FirstOrDefault(x => x.nombreUsuario == usuario && 
                                                    x.Contraseña == contraseña);
                return vigilanteEncontrado;
            }
        }
        
        public Vehiculo ObtenerVehiculoXPlaca(string placa)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {

                var vehiculoEncontrado = contexto.Vehiculos.FirstOrDefault(v => v.Placa == placa);
                return vehiculoEncontrado;
            }
        }

        public void RegistrarVehiculo(Vehiculo vehiculo)
        {

            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {

                contexto.Vehiculos.Add(vehiculo);
                contexto.SaveChanges();
            }
        }
    }
}
