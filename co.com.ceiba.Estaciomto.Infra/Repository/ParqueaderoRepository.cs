﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.EntityFrameworkCore;

namespace co.com.ceiba.Estaciomto.Infra
{
    public class ParqueaderoRepository : RepositoryGeneric<Parqueadero>, IParqueadero
    {
        private readonly DbContextOptionsBuilder<EstacionamientoDBContext> _optionBuilder;

        public ParqueaderoRepository()
        {
            _optionBuilder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
        }

        // constructor para pruebas de integración
        public ParqueaderoRepository(DbContextOptionsBuilder<EstacionamientoDBContext> builder)
        {
            _optionBuilder = builder;

        }

        public int ObtenerNumeroVehiculosActualesEnParqueaderoXTipoVehiculo(string tipoVehiuclo)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    var numeroVehiculosEnParquedero = contexto.Reservasestacionamiento.Where(x => x._vehiculo.TipoVehiculo == tipoVehiuclo
                                                              && x.FechaSalida == DateTime.Parse("0001-01-01T00:00:00")).Count();
                    return numeroVehiculosEnParquedero;
                }
                catch (InvalidOperationException e)
                {

                    return 0;
                }

            }
        }
      
        public ReservaEstacionamiento ObtenerReservaById(Guid Id)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {

                    var resultado = contexto.Reservasestacionamiento.Where(x => x.Id == Id)
                       .Include(x => x._vehiculo).Include(x => x._Parqueadero).FirstOrDefault();
                    return resultado;
                }
                catch (InvalidOperationException e)
                {
                    var error = e.Message;
                    return null;
                }


            }
        }

        public ReservaEstacionamiento ObtenerParqueoDeVehiculoXplaca(string placaVehiculo)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    var resultado = contexto.Reservasestacionamiento.Where(x => x._vehiculo.Placa == placaVehiculo)         
                        .Include(x => x._vehiculo).Include(x => x._Parqueadero).FirstOrDefault();
                    return resultado;
                }
                catch (InvalidOperationException e)
                {
                    var error = e.Message;
                    return null;
                }

            }
        }

        public void IsertarReservaEstacionamientoEnBD(ReservaEstacionamiento reserva)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {

                    contexto.Reservasestacionamiento.Add(reserva);
                    contexto.SaveChanges();
                }
                catch (InvalidOperationException e)
                {
                    throw new InvalidOperationException(e.Message);
                }

            }
        }

        public void actualizarParqueVehiuclo(ReservaEstacionamiento reservaActualizar)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    contexto.Reservasestacionamiento.Update(reservaActualizar);
                    contexto.SaveChanges();
                }
                catch (InvalidOperationException e)
                {
                    throw new InvalidOperationException(e.Message);
                }
            }
        }
    }
}
