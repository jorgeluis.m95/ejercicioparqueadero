﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.EntityFrameworkCore;

namespace co.com.ceiba.Estaciomto.Infra
{
    public class ReservaEstacionamientoRepository : RepositoryGeneric<ReservaEstacionamiento>, IReservaEstacionamiento
    {
        private readonly DbContextOptionsBuilder<EstacionamientoDBContext> _optionBuilder;

        public ReservaEstacionamientoRepository()
        {
            _optionBuilder = new DbContextOptionsBuilder<EstacionamientoDBContext>();

        }

        public double GenerarCobro(DateTime fechaIngreso, DateTime fechaSalida)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ReservaEstacionamiento>> ObtenerReservas()
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return  await contexto.Reservasestacionamiento.Where(x => x.FechaSalida == DateTime.Parse("0001-01-01T00:00:00"))
                    .Include(v => v._vehiculo).Include(v => v._Parqueadero).ToListAsync();
                
                }
                catch (InvalidOperationException e)
                {

                    return null;
                }
                
            }
        }

        public  int ObtenerNumeroReservaActualesMoto()
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return contexto.Reservasestacionamiento.Where(x => x._vehiculo.TipoVehiculo == "Moto").Count();
                }
                catch (InvalidOperationException e)
                {

                    return 0;
                }
            }
        }
        public int ObtenerNumeroReservaActualesCarro()
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return contexto.Reservasestacionamiento.Where(x => x._vehiculo.TipoVehiculo == "Carro").Count();
                }
                catch (InvalidOperationException e)
                {

                    return 0;
                }
                
            }
        }

        public ReservaEstacionamiento ObtenerReservaDeVehiculo(Guid IdVehiculo)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    var resultado = contexto.Reservasestacionamiento.Where(x => x.VehiculoId == IdVehiculo 
                                       // && x.FechaSalida == DateTime.Parse("0001-01-01T00:00:00"))
                                        ).Include(x => x._vehiculo).Include(x => x._Parqueadero).FirstOrDefault();
                    return resultado;
                }
                catch (InvalidOperationException e)
                {
                    var error = e.Message;
                    return null;
                }

            }
        }

        public string ValidarDiaSemana(int numeroDia)
        {
            throw new NotImplementedException();
        }

        public string ValidarPlaca(string placa)
        {
            throw new NotImplementedException();
        }

        public void validarTipoCobro(string tipoVehiculo)
        {
            throw new NotImplementedException();
        }

        public ReservaEstacionamiento ObtenerReservaById(Guid Id)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {

                    var resultado = contexto.Reservasestacionamiento.Where(x => x.Id == Id)
                       .Include(x => x._vehiculo).Include(x => x._Parqueadero).FirstOrDefault();
                    return resultado;
                }
                catch (InvalidOperationException e)
                {
                    var error = e.Message;
                    return null;
                }
                
                    
            }
        }
    }
}
