﻿using System;
using System.Collections.Generic;
using System.Text;
using co.com.ceiba.Estaciomto.Domain;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


namespace co.com.ceiba.Estaciomto.Infra
{
    public class RepositoryGeneric<T> : IRepositoryGeneric<T>, IDisposable where T : class
    {

        private DbContextOptionsBuilder<EstacionamientoDBContext> _optionBuilder;


        public RepositoryGeneric()
        {
            _optionBuilder = new DbContextOptionsBuilder<EstacionamientoDBContext>();
        }

        ~RepositoryGeneric()
        {
            Dispose(false);
        }

        public void Add(T Entitie)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                
                try
                {
                    contexto.Add(Entitie);
                    contexto.SaveChanges();
                }
                catch (Exception e)
                {
                    var err = e.Message;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public T GetById(Guid id)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return  contexto.Set<T>().Find(id);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public async Task<List<T>> ListAll()
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    return await  contexto.Set<T>().ToListAsync();
                }
                catch (InvalidOperationException e)
                {
                    throw;
                }
            }
        }

        public void Update(T Entitie)
        {
            using (EstacionamientoDBContext contexto = new EstacionamientoDBContext(_optionBuilder.Options))
            {
                try
                {
                    contexto.Update(Entitie);
                    contexto.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        private void Dispose(bool Status)
        {
            if (!Status) return;
        }
    }
}
