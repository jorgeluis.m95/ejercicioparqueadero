﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using _entities = co.com.ceiba.Estaciomto.Domain;

namespace co.com.ceiba.Estaciomto.Infra
{
    public class EstacionamientoDBContext : DbContext
    {
        public IConfigurationRoot Configuration { get; set; }
        public EstacionamientoDBContext(DbContextOptions<EstacionamientoDBContext>options): base(options)
        {
            
            Database.EnsureCreated();
        }



        public DbSet<_entities.Vigilante> Vigilantes { get; set; }

        public DbSet<_entities.Parqueadero> Parqueaderos { get; set; }

        public DbSet<_entities.Vehiculo> Vehiculos { get; set; }

        public DbSet<_entities.ReservaEstacionamiento> Reservasestacionamiento { get; set; }

        public DbSet<_entities.Precio> Precios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(RetornaUrlConection());
                
            }
        }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            base.OnModelCreating(modelbuilder);
            modelbuilder.Entity<_entities.ReservaEstacionamiento>().HasOne(x => x._vehiculo)
                .WithMany(x => x.ListReservaEstacionamientos)
                .HasForeignKey(x => x.VehiculoId);

            modelbuilder.Entity<_entities.ReservaEstacionamiento>().HasOne(x => x._Parqueadero)
                .WithMany(x => x.ListReservaEstacionamientos)
                .HasForeignKey(x => x.ParqueaderoId);

            base.OnModelCreating(modelbuilder);
        }

        public  string RetornaUrlConection()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            string conn = Configuration.GetConnectionString("DefaultConnection");
            return conn;
        }

    }
}
